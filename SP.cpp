#include <iostream>

using namespace std;

class Some_Class{
    public:
        int field_i;
        char field_c;
        Some_Class(const int a, const char b):field_i(a), field_c(b) {
            cout << "Constructor Some Class!" << endl;
        }
        ~Some_Class() {
            cout << "Destructor Some Class!" << endl;
        }
};

template <typename T>
class Smart_Point{
    private:
        T* point;

    public:

        Smart_Point(T addres): point(& addres) {
            cout << "Pointer constructor!" << endl;
        }

        ~Smart_Point() {
            cout << "Pointer destructor!" << endl;
            if (point) delete point;
        }

        friend ostream& operator<<(ostream& pstream, const Smart_Point<Some_Class>& obj);

        T operator* () {
            cout << "* overload!" << endl;
            return *point;
        }

        T* operator-> () {
            cout << "-> overload!" << endl;
            return point;
        }

        T* operator& () {
            cout << "& overload!" << endl;
            return point;
        }



};


ostream& operator<<(ostream& pstream, const Some_Class& obj) {
    pstream << "Field_c: " <<  obj.field_c << ", field_i: " << obj.field_i << endl;
    return pstream;
}


int main()
{
    //cout << "Hello world!" << endl;
    Some_Class obj(10, 'a');
    Smart_Point<Some_Class> p1(obj);
    cout << *p1 << endl;
    cout << &p1 << endl;
    cout << p1->field_c << endl;
    return 0;
}

#include <iostream>
#include <fstream>
#include <string>
#include <string.h>
#include <regex>
#include <vector>

using namespace std;


struct data
{
    string name_section;
    vector<string> fields;
};

class IniParsing
{
    private:
            int count_section = 0;
            vector<string> array_section;
            vector<string> array_fields;
            vector<data> all_data;

            string ignor_comments(string str) {
                string result = "";
                for (int i = 0; i < str.length(); i++) {
                    if ((str[i] == ';') || (str[i] == '#'))
                        break;
                    else
                        result += str[i];

                }
                return result;
            }

    public:
            IniParsing(string name)
                {

                string buff;
                string after_buff;
                bool flag = false;
                bool twin = false;

                // *** For regular ***
                cmatch result;
                regex regular("\\["
                              "[\\w]*"
                              "\\]");
                // *******************

                cout << "- - Reading file - -" << endl;
                ifstream ini(name.c_str(), ios_base::in);

                if (!ini.is_open())
                    cout  << "Error: file can't open";
                else
                {
                    while (!ini.eof())
                    {
                        getline(ini, buff);

                        // Ignoring empty strings and comments
                        if ((buff.length() == 0) ||
                            (buff[0] == ';') ||
                            (buff[0] == '#'))
                            continue;
                        else buff = ignor_comments(buff);

                        // Create array of sections
                        if (regex_match(buff.c_str(), result, regular))
                            {
                                count_section += 1;
                                if (count_section > 1)
                                {
                                    // Data storage
                                    data my_data;
                                    my_data.name_section = array_section[array_section.size()-1];
                                    my_data.fields = array_fields;
                                    all_data.insert(all_data.end(), my_data);
                                    array_fields.clear();
                                }

                                array_section.insert(array_section.end(), buff);
                                continue;
                            }

                        // If string ends on symbol \ - the lines below need to be remembered
                        if (buff[buff.length()-1] == '\\')  {
                            flag = true;
                            after_buff = after_buff + " " + buff;
                        } else {
                            after_buff = after_buff + " " + buff;
                            // Block duplicate fields
                            bool need_add = true;
                            for(int i = 0; i < array_fields.size(); i++) {
                                if (array_fields[i] == after_buff) need_add = false;
                            }
                            if (need_add) array_fields.push_back(after_buff);
                            // **********************
                            flag = false;
                            after_buff = "";
                        }
                    };
                    data my_data;
                    my_data.name_section = array_section[array_section.size()-1];
                    my_data.fields = array_fields;
                    all_data.insert(all_data.end(), my_data);

                    ini.close();
                }
                cout << "Reading ended successfully" << endl;
                cout << "- - - - - - - - - -  - -\n" << endl;
            }

            void print_sections(){
                cout << "- - Printing section - -" << endl;
                for(int i = 0; i < array_section.size(); i++)
                    cout << array_section[i] << "\n";
                cout << "- - - - - - - - - -  - -\n" << endl;
            }

            void show_all(){
                cout << "- - Printing all information - -" << endl;
                for(int i = 0; i < all_data.size(); i++) {
                    cout << all_data[i].name_section << "\n";
                    for(int j = 0; j < all_data[i].fields.size(); j++){
                        cout << all_data[i].fields[j] << "\n";
                    }
                }
                cout << "- - - - - - - - - -  - -\n" << endl;
            }

            void print_one_section(){
                cout << "- - About section - -" << endl;
                string choice;
                cout << "Choose section: ";
                cin >> choice;
                choice = "[" + choice + "]";
                bool search = false;
                for(int i = 0; i < array_section.size(); i++) {
                    if (array_section[i] == choice) {
                        for(int j = 0; j < all_data[i].fields.size(); j++){
                        cout << all_data[i].fields[j] << "\n";
                    }
                    }
                }
                if (search == false) cout << "Section not found." << endl;;
                cout << "- - - - - - - - - -  - -\n" << endl;
            }
};

class u_map {};

int main(void)
{
    IniParsing parsing("pam.ini");
    parsing.print_sections();
    parsing.show_all();
    parsing.print_one_section();

    return 0;
}
